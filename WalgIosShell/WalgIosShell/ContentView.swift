//
//  ContentView.swift
//  WalgIosShell
//
//  Created by Abhishek Sharma on 06/01/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
