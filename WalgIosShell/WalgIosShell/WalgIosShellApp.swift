//
//  WalgIosShellApp.swift
//  WalgIosShell
//
//  Created by Abhishek Sharma on 06/01/22.
//

import SwiftUI

@main
struct WalgIosShellApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
