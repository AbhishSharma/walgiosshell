@_private(sourceFile: "ContentView.swift") import WalgIosShell
import SwiftUI
import SwiftUI

extension ContentView_Previews {
    @_dynamicReplacement(for: previews) private static var __preview__previews: some View {
        #sourceLocation(file: "/Users/abhsharm6/Documents/AbhishekSharma/Projects/Walgreens/CICD/iosProject/walgiosshell/WalgIosShell/WalgIosShell/ContentView.swift", line: 19)
        ContentView()
    #sourceLocation()
    }
}

extension ContentView {
    @_dynamicReplacement(for: body) private var __preview__body: some View {
        #sourceLocation(file: "/Users/abhsharm6/Documents/AbhishekSharma/Projects/Walgreens/CICD/iosProject/walgiosshell/WalgIosShell/WalgIosShell/ContentView.swift", line: 12)
        Text(__designTimeString("#3620.[1].[0].property.[0].[0].arg[0].value", fallback: "Hello, world!"))
            .padding()
    #sourceLocation()
    }
}

import struct WalgIosShell.ContentView
import struct WalgIosShell.ContentView_Previews